 
$( document ).ready(function() {
    $( "#dark-theme" ).click(function() {
        $( ".container.header" ).toggleClass( "open" );
        $( "#navbarsExampleDefault" ).toggleClass( "show-mobile-menu" );
      });

      
      $( ".search-title" ).click(function() {
        $( ".form" ).addClass( "open-search" );
      });

      
      $( ".order-call" ).click(function() {
        $( ".overlay" ).addClass( "show-overlay" );
      });
      $( ".popup-close" ).click(function() {
        $( ".overlay" ).removeClass( "show-overlay" );
      });

      $( "#success" ).click(function() {
        $( ".popup-order-call" ).addClass( "hide-form" );
        $( ".popup-order-success" ).addClass( "show-form" );
      });
      
      $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".open-search"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
                //div.hide(); // скрываем его
                $( div).removeClass( "open-search" );
        }
      });
      $(document).mouseup(function (e){ // событие клика по веб-документу
        
        var div = $(".show-overlay .popup-order-call"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
                //div.hide(); // скрываем его
                //$(".overlay").removeClass( "show-overlay" );
                $(".overlay").removeClass('show-overlay');
                $( ".popup-order-call" ).removeClass( "hide-form" );
                $( ".popup-order-success" ).removeClass( "show-form" );
        }
      });

      
});
